
from django.contrib import admin
from django.urls import path, include
from ppiZol.views import AnimalViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register('animais', AnimalViewSet)

urlpatterns = [
	path('', include('ppiZol.urls')),
	path('animais/', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls'))
]
