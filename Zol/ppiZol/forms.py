from django import forms
from .models import Animal

class AnimalForm(forms.ModelForm):
    nome_animal = forms.CharField(max_length=50)
    peso = forms.IntegerField()
    idade = forms.IntegerField()
    class Meta:
        model = Animal
        fields = ['nome_animal', 'peso', 'idade']