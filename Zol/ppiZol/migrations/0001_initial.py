# Generated by Django 2.2.7 on 2019-11-23 16:04

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Animal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome_animal', models.CharField(max_length=50)),
                ('peso', models.IntegerField()),
                ('idade', models.IntegerField()),
            ],
        ),
    ]
