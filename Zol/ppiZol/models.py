from django.db import models


class Animal(models.Model):

	nome_animal = models.CharField(max_length=50)
	peso = models.IntegerField()
	idade = models.IntegerField()

	def __str__(self):
		return self.nome_animal