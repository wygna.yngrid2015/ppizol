from django.urls import path
from .views import listar_animais, update_animais, delete

urlpatterns = [
    path('', listar_animais, name='listar_animais'),
    path('update/<int:id>/', update_animais, name='update_animais'),
    path('delete/<int:pk>/', delete, name="delete"),
]