from django.shortcuts import render, redirect
from rest_framework import viewsets
from .models import Animal
from .serializers import AnimalSerializer
from .forms import AnimalForm

class AnimalViewSet(viewsets.ModelViewSet):
	queryset = Animal.objects.all()
	serializer_class = AnimalSerializer

def listar_animais(request):
    animal = Animal.objects.all()
    return render(request, 'listagem.html', {'animal': animal})


def update_animais(request, id):
    animal = Animal.objects.get(id=id)
    form = AnimalForm(request.POST or None, instance=animal)

    if form.is_valid():
        form.save()
        return redirect('listar_animais')

    return render(request, 'edicao.html', {'form': form})


def delete (request, pk):
    animal = Animal.objects.get(pk=pk)
    animal.delete()
    return redirect('listar_animais')
